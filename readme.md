# Frontend Mentor - QR code component solution

This is my solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H).\
[See it live](https://russofrancesco.gitlab.io/frontend-mentor-qr-code-component/).

## Screenshots

<div align="center">
  <img src="./solution_screenshots/mobile.png" height="320"/>
  <img src="./solution_screenshots/desktop.png" height="320"/>
</div>

## Built with

- semantic HTML5
- vanilla CSS
